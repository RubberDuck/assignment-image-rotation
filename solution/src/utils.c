//
// Created by Stepan on 29.10.2023.
//
#include "../include/utils.h"

uint32_t count_padding(uint32_t width) {
    uint32_t bytes_in_row = width * sizeof(struct pixel);
    return (4 - (bytes_in_row % 4)) % 4;
}
