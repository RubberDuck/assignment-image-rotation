//
// Created by Stepan on 29.10.2023.
//
#include "../include/file_io.h"
#include "../include/input_formats/all.h"
#include <stdio.h>
#include <stdlib.h>

#include "../include/utils.h"


//Read BMP-header and image from file
enum READ_FILE_STATUS read_bmp_file(const char *input_path, struct BMPHeader *header, struct image *image) {
    FILE *file = fopen(input_path, "rb");
    if (!file){
        free(header);
        free(image);
        return FILE_READ_INCORRECT_PATH;
    }

    fread(header, sizeof(struct BMPHeader), 1, file);

    image->height = header->biHeight;
    image->width = header->biWidth;
    image->data = malloc(image->height * image->width * sizeof(struct pixel));

    if (!image->data){
        free(image->data);
        free(image);
        free(header);
        return FILE_READ_MEMORY_ERROR;
    }

    uint32_t padding = count_padding(image->width);
    for (size_t i = 0; i < image->height; i++) {
        fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, file);
        fseek(file, (long) padding, SEEK_CUR);
    }
    fclose(file);
    return FILE_READ_OK;
}

//Write BMP-header and image to file
enum WRITE_FILE_STATUS write_in_bmp_file(const char *output_path, struct BMPHeader *header, struct image *image) {
    FILE *file = fopen(output_path, "wb");
    if (!file) {
        free(header);
        free(image);
        return FILE_WRITE_INCORRECT_PATH;
    }

    fwrite(header, sizeof(struct BMPHeader), 1, file);

    uint32_t padding = count_padding(image->width);
    for (size_t i = 0; i < image->height; i++){
        fwrite(&image->data[i*image->width], image->width * sizeof(struct pixel), 1, file);
        for(size_t j = 0; j < padding; j++){
            fputc(0, file);
        }
    }
    fclose(file);
    return FILE_WRITE_OK;
}

