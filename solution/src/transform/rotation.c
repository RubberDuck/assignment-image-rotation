//
// Created by Stepan on 29.10.2023.
//
#include "../../include/transform/rotation.h"

struct image rotate(struct image *image){
    struct image *new_image = malloc(sizeof(struct image));
    if(!new_image) return (struct image){0,0,NULL};
    new_image->height = image->width;
    new_image->width = image->height;
    new_image->data = malloc(new_image->height * new_image->width * sizeof(struct pixel));

    if(!new_image->data){
        free(new_image);
        return (struct image){0,0,NULL};
    }
    for(size_t y = 0; y < new_image->height; y++){
        for(size_t x = 0; x < new_image->width; x++){
            new_image->data[new_image->width * y + x] = image->data[(image->height - 1 - x) * image->width+y];
        }
    }
    free(image->data);
    *image = *new_image;
    free(new_image);
    return *image;
}

