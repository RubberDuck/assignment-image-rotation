#include "../include/file_io.h"
#include <stdio.h>

#include "../include/transform/all.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    if (argc > 3){
        fprintf(stderr, "Too many parameters (3 expected)!\n");
        return 1;
    }
    else if(argc < 3) {
        fprintf(stderr, "Not enough parameters (3 expected)!\n");
        return 1;
    }

    const char* input_path = argv[1];
    const char* output_path = argv[2];

    struct BMPHeader* input_header = malloc(sizeof(struct BMPHeader));
    if (input_header == NULL) {
        free(input_header);
        fprintf(stderr, "Error with memory allocation");
        return 1;
    }
    struct image* input_image = malloc(sizeof(struct image));
    if (input_image == NULL) {
        free(input_image);
        free(input_header);
        fprintf(stderr, "Error with memory allocation");
        return 1;
    }

    if(read_bmp_file(input_path, input_header, input_image) != FILE_READ_OK){
        fprintf(stderr, "Error with reading from file");
        return 1;
    }

    struct BMPHeader* output_header;

    output_header = input_header;
    output_header->biWidth = input_image->height;
    output_header->biHeight = input_image->width;
    *input_image = rotate(input_image);

    if (!input_image->data) {
        free(input_image);
        free(input_header);
        fprintf(stderr, "Error with malloc");
        return 1;
    }

    if(write_in_bmp_file(output_path, output_header, input_image) != FILE_WRITE_OK){
        free(input_image->data);
        free(input_image);
        free(input_header);
        fprintf(stderr, "Error with writing to file");
        return 1;
    }
    free(input_image->data);
    free(input_image);
    free(input_header);

    return 0;
}
