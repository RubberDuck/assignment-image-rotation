//
// Created by Stepan on 29.10.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_INTERNAL_FORMAT_H
#define ASSIGNMENT_IMAGE_ROTATION_INTERNAL_FORMAT_H

#include  <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_INTERNAL_FORMAT_H
