//
// Created by Stepan on 29.10.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATION_H

#include "../internal_format.h"
#include <stdlib.h>

struct image rotate(struct image *image);
enum CREATE_IMAGE_STATUS{
    CREATE_SUCCESS = 0,
    CREATE_FAILED
};
#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
