//
// Created by Stepan on 29.10.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "../internal_format.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) BMPHeader{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

enum READ_BMP_STATUS{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum READ_BMP_STATUS from_bmp( FILE* in, struct image* img );

enum WRITE_BMP_STATUS{
    WRITE_OK = 0,
    WRITE_INCORRECT_PATH,
    WRITE_ACCESS_ERROR,
    WRITE_WRONG_HEADER
};

enum WRITE_BMP_STATUS to_bmp( FILE* out, struct image const* img );

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
