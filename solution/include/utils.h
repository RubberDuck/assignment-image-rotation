//
// Created by Stepan on 29.10.2023.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include "./internal_format.h"
#include <inttypes.h>


uint32_t count_padding(uint32_t width);


#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_H
