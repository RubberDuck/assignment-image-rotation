//
// Created by Stepan on 29.10.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H

#include "./input_formats/all.h"

enum READ_FILE_STATUS{
    FILE_READ_OK = 0,
    FILE_READ_INCORRECT_PATH,
    FILE_READ_ACCESS_ERROR,
    FILE_READ_MEMORY_ERROR,
    FILE_READ_INVALID_HEADER,
    FILE_READ_INVALID_BITS
};

enum READ_FILE_STATUS read_bmp_file(const char *input_path, struct BMPHeader *header, struct image *image);

enum WRITE_FILE_STATUS{
    FILE_WRITE_OK = 0,
    FILE_WRITE_INCORRECT_PATH,
    FILE_WRITE_ACCESS_ERROR,
    FILE_WRITE_WRONG_HEADER
};

enum WRITE_FILE_STATUS write_in_bmp_file(const char *output_path, struct BMPHeader *header, struct image *image);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_IO_H
